with User_Directories;   use User_Directories;

procedure Main is
   Dir : User_Directory;
begin
   Add_User (Dir => dir,
             ID  => "123456",
             SSN => "ABCDEF64X12Q456R");

   Add_User (Dir => dir,
             ID  => "654321",
             SSN => "XBCDEF64X12Q456R");

   Add_User (Dir => dir,
             ID  => "654320",
             SSN => "XXCDEF64X12Q456R");

   Delete (Dir, Id => "123456");
   Delete (Dir, SSN => "XXCDEF64X12Q456R");
end Main;
