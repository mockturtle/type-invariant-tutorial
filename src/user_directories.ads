with Ada.Containers.Ordered_Maps;

package User_Directories is
   subtype ID_Char is Character range '0' .. '9';
   type Serial_ID is array (1 .. 6) of ID_Char;

   type User_SSN is new String (1 .. 16);

   type User_Directory is private;

   procedure Add_User (Dir : in out User_Directory;
                       ID  : in Serial_ID;
                       SSN : in User_SSN)
     with
       Pre => not Contains (Dir, Id) and not Contains (Dir, Ssn),
     Post =>  Contains (Dir, Id) and Contains (Dir, Ssn);

   function Contains (Dir : User_Directory;
                      ID  : Serial_ID)
                      return Boolean;

   function Contains (Dir  : User_Directory;
                      SSN  : User_SSN)
                      return Boolean;

   procedure Delete (Dir : in out User_Directory;
                     ID  : in Serial_ID)
     with
       Pre => Contains (Dir, Id),
     Post =>  not Contains (Dir, Id);


   procedure Delete (Dir  : in out User_Directory;
                     SSN  : in User_SSN)
     with
       Pre => Contains (Dir, SSN),
     Post =>  not Contains (Dir, SSN);

private

   type User_Record is
      record
         ID  : Serial_ID;
         SSN : User_SSN;
      end record;

   type User_Record_Pt is access User_Record;

   package ID_To_User_Maps is
     new Ada.Containers.Ordered_Maps (Key_Type     => Serial_ID,
                                      Element_Type => User_Record_Pt);

   package SSN_To_User_Maps is
     new Ada.Containers.Ordered_Maps (Key_Type     => User_SSN,
                                      Element_Type => User_Record_Pt);

   function Is_Coherent (Dir : User_Directory) return Boolean;


   type User_Directory is
      record
         ID_To_User  : ID_To_User_Maps.Map;
         SSN_To_User : SSN_To_User_Maps.Map;
      end record
     with Type_Invariant => Is_Coherent (User_Directory);
end User_Directories;
