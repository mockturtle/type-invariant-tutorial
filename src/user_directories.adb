pragma Ada_2012;

with Ada.Unchecked_Deallocation;

package body User_Directories is

   procedure Free is
     new Ada.Unchecked_Deallocation (Object => User_Record,
                                     Name   => User_Record_Pt);

   --------------
   -- Add_User --
   --------------

   procedure Add_User
     (Dir : in out User_Directory; ID : in Serial_ID; SSN : in User_SSN)
   is
      New_User : constant User_Record_Pt := new User_Record'(ID  => ID,
                                                             SSN => SSN);
   begin
      if Dir.ID_To_User.Contains (ID) then
         raise Constraint_Error;
      end if;

      Dir.ID_To_User.Include (Key      => Id,
                              New_Item => New_User);


      Dir.SSN_To_User.Include (Key      => SSN,
                               New_Item => New_User);
   end Add_User;

   --------------
   -- Contains --
   --------------

   function Contains (Dir : User_Directory; ID : Serial_ID) return Boolean
   is (Dir.ID_To_User.Contains (ID));


   --------------
   -- Contains --
   --------------

   function Contains (Dir : User_Directory; SSN : User_SSN) return Boolean
   is (Dir.SSN_To_User.Contains (SSN));

   ------------
   -- Delete --
   ------------

   procedure Delete (Dir : in out User_Directory; ID : in Serial_ID) is
      To_Be_Removed : User_Record_Pt := Dir.ID_To_User (Id);
   begin
      Dir.SSN_To_User.Delete (To_Be_Removed.SSN);
      Dir.ID_To_User.Delete (Id);

      Free (To_Be_Removed);
   end Delete;

   ------------
   -- Delete --
   ------------

   procedure Delete (Dir : in out User_Directory; SSN : in User_SSN) is
      To_Be_Removed : User_Record_Pt := Dir.SSN_To_User (SSN);
   begin
      Dir.ID_To_User.Delete (To_Be_Removed.Id);
      Dir.SSN_To_User.Delete (SSN);

      Free (To_Be_Removed);
   end Delete;

   -----------------
   -- Is_Coherent --
   -----------------

   function Is_Coherent (Dir : User_Directory) return Boolean is
      use Ada.Containers;
      use ID_To_User_Maps;
      use SSN_To_User_Maps;
   begin
      if Dir.ID_To_User.Length /= Dir.SSN_To_User.Length then
         return False;
      end if;

      for Pos in Dir.ID_To_User.Iterate loop
         declare
            Usr_Entry : constant User_Record_Pt := Element (Pos);
         begin
            if Usr_Entry /= Dir.SSN_To_User (Usr_Entry.Ssn) then
               return False;
            end if;

            if Key (Pos) /= Usr_Entry.Id then
               return False;
            end if;
         end;
      end loop;

      return True;
   end Is_Coherent;

end User_Directories;
